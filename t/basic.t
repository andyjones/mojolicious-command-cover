use Test::Most;

use_ok('Mojolicious::Command::cover');

my $cover = Mojolicious::Command::cover->new;
ok $cover->description, 'has a description';
ok $cover->usage,       'has usage information';

done_testing;
