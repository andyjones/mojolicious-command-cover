use strict;
use warnings;

package Mojolicious::Command::cover;

# ABSTRACT: Run tests with coverage

use Mojo::Base 'Mojolicious::Command::test';

has description => "Run unit tests with Devel::Cover.\n";
has usage       => <<EOF;
usage: $0 cover [OPTIONS] [TESTS]

These options are available:
  -v, --verbose   Print verbose debug information to STDERR.
EOF

has ignore_extensions => sub {
  return ['.tt.ttc'];
};

has ignore_re => sub {
  return [qr{^t/}];
};

# maybe use around?
sub run {
  my ($self, @args) = @_;

  $self->load_devel_cover;

  # cover -delete
  $self->SUPER::run(@args);
  # cover
}

sub load_devel_cover {
  my ($self) = @_;

  require Devel::Cover;
  Devel::Cover->import($self->devel_cover_import);
}

sub devel_cover_import {
  my ($self) = @_;

  my $ignore_extensions = join '|', map { quotemeta($_) } @{ $self->ignore_extensions };

  my @ignores;
  push @ignores, "*\.($ignore_extensions)\$" if $ignore_extensions;
  push @ignores, @{ $self->ignore_re };

  return join ',', map { "+ignore,$_" } grep { $_ } @ignores;
}

1;

__END__

=head1 SYNOPSIS

  use Mojolicious::Command::cover;

  my $cover = Mojolicious::Command::cover->new;
  $test->run(@ARGV);

=head1 DESCRIPTION

L<Mojolicious::Command::cover> runs application tests from the C<t>
directory under L<Devel::Cover>.

=head1 ATTRIBUTES

L<Mojolicious::Command::cover> inherits all attributes from
L<Mojolicious::Command::Test> and implements the following new ones.

=head2 description

  my $description = $cover->description

Provides a short description to the command list.

=head2 usage

  my $usage = $cover->usage;

Provides usage information for this command to the help screen.

=head1 METHODS

L<Mojolicious::Command::cover> inherits all methods from
L<Mojolicious::Command::test> and implements the following new ones.

=head2 run

  $cover->run(@ARGV);

Run this command.

=head1 SEE ALSO

L<Mojolicious>, L<Mojolicious::Command::test>, L<http://mojolicio.us>

=cut
